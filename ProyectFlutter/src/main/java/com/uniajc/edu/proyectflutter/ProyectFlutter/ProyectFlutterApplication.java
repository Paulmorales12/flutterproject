package com.uniajc.edu.proyectflutter.ProyectFlutter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectFlutterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectFlutterApplication.class, args);
	}

}
